require 'test_helper'

class CreateRepairCommentActionTest < ActiveSupport::TestCase
  
  test 'can be performed' do
    repair = create(:repair)
    author = create(:user)

    args = attributes_for(:repair_comment)
    args[:repair_id] = repair.id

    comment = CreateRepairCommentAction.new(args, user: author).call

    assert_equal(args[:text], comment.text)
    assert_equal(args[:repair_id], comment.repair_id)
    assert_equal(author.id, comment.author_id)
  end

  test 'requires auth' do
    assert_raises(ApplicationAction::AuthError){
      CreateRepairCommentAction.new({}, user: nil).call
    }
  end

end