require 'test_helper'

class UpdateRepairActionTest < ActiveSupport::TestCase
  
  test 'can be updated by Manager' do
    repair = create(:repair)
    manager = create(:user, role: 'manager')
    args = {
      id: repair.id,
      description: 'Some other description',
      status: 'complete'
    }
    repair = UpdateRepairAction.new(args, user: manager).call
    assert_equal(args[:description], repair.description)
    assert_equal(args[:status], repair.status)
  end

  test 'can be flagged as complete by User' do
    repair = create(:repair)
    user = create(:user)
    args = {
      id: repair.id,
      description: 'Some other description',
      status: 'complete'
    }
    repair = UpdateRepairAction.new(args, user: user).call
    assert_not_equal(args[:description], repair.description)
    assert_equal(args[:status], repair.status)
  end

  test 'requires auth' do
    assert_raises(ApplicationAction::AuthError){
      UpdateRepairAction.new({}, user: nil).call
    }
  end

end