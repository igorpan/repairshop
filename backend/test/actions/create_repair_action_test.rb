require 'test_helper'

class CreateRepairActionTest < ActiveSupport::TestCase
  
  test 'can be performed by manager only' do
    args = attributes_for(:repair)
    args[:assignee] = create(:user)
    [nil, User.new].each do |user|
      assert_raises(ApplicationAction::AuthError){
        CreateRepairAction.new(args, { user: user }).call
      }
    end
    repair = CreateRepairAction.new(args, { user: User.new(role: 'manager') }).call
    assert_equal(args[:description], repair.description)
    assert_equal(args[:time], repair.time)
    assert_equal(args[:assignee], repair.assignee)
  end

end