FactoryGirl.define do
  factory :repair do
    time Time.new(2017, 9, 16, 3, 59, 31)
    description 'Replace clutch'
    status 'incomplete'
    assignee factory: :user
  end

  factory :repair_comment, class: 'Comment' do
    text 'New transmission is better than the old one'
    repair
    author factory: :user
  end

  factory :user do
    name 'John User'
    role 'user'
    sequence(:email){ |i| "igor_#{i}@pantovic.com" }
    password{ SecureRandom.hex(8) }
  end
  
end