require 'test_helper'

class RepairTest < ActiveSupport::TestCase
  test 'defaults' do
    Timecop.freeze do
      repair = Repair.new
      assert_equal(Time.current, repair.time)
      assert_equal('incomplete', repair.status)
      assert_equal('', repair.description)
    end
  end

  test 'time availability validation' do
    scheduled_repair_time = Time.new(2017, 1, 1, 10, 0, 0)
    scheduled_repair = create(:repair, time: scheduled_repair_time)

    overlapping_repairs = [
      build(:repair, time: scheduled_repair_time + 59.minutes),
      build(:repair, time: scheduled_repair_time - 59.minutes)
    ]
    not_overlapping_repairs = [
      build(:repair, time: scheduled_repair_time + 1.hour),
      build(:repair, time: scheduled_repair_time - 1.hour)
    ]

    overlapping_repairs.each{ |repair| 
      assert(repair.save == false)
      assert_includes(repair.errors.details[:time], error: :time_availability)
    }

    not_overlapping_repairs.each{ |repair|
      assert(repair.save == true)
    }
  end
end
