require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'can be found by JWT' do
    user = create(:user)
    user_for_jwt = User.for_jwt(user.jwt)
    assert_equal(user, user_for_jwt)
  end

  test 'raises TokenInvalidError for invalid JWT token' do
    assert_raises(CredentialsJWT::TokenInvalidError){
      User.for_jwt('somethinginvalid')
    }
  end

  test 'raises TokenExpiredError for expired tokens' do
    user = create(:user)
    jwt = user.jwt
    Timecop.travel(1.year.from_now) do
      assert_raises(CredentialsJWT::TokenExpiredError){
        User.for_jwt(jwt)
      }
    end
  end

  test 'raises NotFoundError for unexisting credentials' do
    user = create(:user)
    jwt = user.jwt
    user.destroy!
    assert_raises(CredentialsJWT::NotFoundError){
      User.for_jwt(jwt)
    }
  end

  test 'jwt generation fails if credentials has no id' do
    assert_raises(CredentialsJWT::TokenGenerationError){
      User.new.jwt
    }
  end
end
