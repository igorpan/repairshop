GraphqlSchema = GraphQL::Schema.define do
  mutation(GraphqlTypes::MutationType)
  query(GraphqlTypes::QueryType)
end
