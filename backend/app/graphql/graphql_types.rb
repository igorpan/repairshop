module GraphqlTypes
  @wrap_fns = {
    must_auth: ->(resolve){
      ->(obj, args, ctx){
        if ctx[:user].present?
          resolve.call(obj, args, ctx)
        else
          nil
        end
      }
    }
  }

  class << self
    def def_type(&block)
      GraphQL::ObjectType.define(&block)
    end

    def def_interface_type(&block)
      GraphQL::InterfaceType.define(&block)
    end
    
    def compose_resolve(*wrappers, resolve)
      composed = resolve
      wrappers.each do |wrapper|
        composed = @wrap_fns[wrapper].call(composed)
      end
      composed
    end
  end

  TimeType = GraphQL::ScalarType.define do
    name 'Time'
    description 'Time in ISO8601 format'
  
    coerce_input ->(value, ctx) { Time.iso8601(value) }
    coerce_result ->(value, ctx) { value.iso8601 }
  end
end