class GraphqlAction
  def initialize(action_klass)
    @action_klass = action_klass
  end

  def call(obj, args, ctx)
    @action_klass.new(args.to_h.with_indifferent_access, ctx).call
  end
end