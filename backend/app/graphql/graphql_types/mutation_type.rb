module GraphqlTypes
  MutationType = def_type do
    name 'Mutation'
  
    field :create_repair_comment, RepairCommentType do
      description 'Comment on a repair'
  
      argument :repair_id, !types.ID
      argument :text, !types.String

      resolve GraphqlAction.new(CreateRepairCommentAction)
    end

    field :update_repair, RepairType do
      description 'Update repair with given ID'

      argument :id, !types.ID
      argument :description, types.String
      argument :time, TimeType
      argument :status, types.String
      argument :assignee_id, types.ID

      resolve GraphqlAction.new(UpdateRepairAction)
    end

    field :create_repair, RepairType do
      description 'Create repair'

      argument :description, !types.String
      argument :time, !TimeType
      argument :assignee_id, types.ID
      argument :status, !types.String

      resolve GraphqlAction.new(CreateRepairAction)
    end

    field :create_user, UserType do
      description 'Create user'

      argument :name, !types.String
      argument :role, !types.String
      argument :email, !types.String
      argument :password, !types.String

      resolve GraphqlAction.new(CreateUserAction)
    end

    field :update_user, UserType do
      description 'Update user'

      argument :id, !types.ID
      argument :name, types.String
      argument :role, types.String
      argument :email, types.String
      argument :password, types.String

      resolve GraphqlAction.new(UpdateUserAction)
    end

    field :delete_user, UserType do
      description 'Delete user'

      argument :id, !types.ID

      resolve GraphqlAction.new(DeleteUserAction)
    end

    field :delete_repair, RepairType do
      description 'Delete repair'

      argument :id, !types.ID

      resolve GraphqlAction.new(DeleteRepairAction)
    end
  end
end