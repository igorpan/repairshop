module GraphqlTypes
  QueryType = def_type do
    name 'Query'

    field :repairs do
      type types[!RepairType]
      description 'List all accessable repairs'

      argument :from_time, TimeType
      argument :to_time, TimeType
      argument :assignee_id, types.ID
      argument :status, types.String

      resolve GraphqlTypes.compose_resolve(
        :must_auth,
        ->(obj, args, ctx){
          scope = ctx[:user].accessible_repairs
          if args[:from_time]
            scope = scope.where('time >= ?', args[:from_time])
          end
          if args[:to_time]
            scope = scope.where('time <= ?', args[:to_time])
          end
          if args[:assignee_id]
            scope = scope.where(assignee_id: args[:assignee_id])
          end
          if args[:status]
            scope = scope.where(status: args[:status])
          end
          scope
        }
      )
    end

    field :repair do
      type RepairType
      argument :id, !types.ID
      description 'Find a Repair by ID'

      resolve GraphqlTypes.compose_resolve(
        :must_auth,
        ->(obj, args, ctx){ 
          ctx[:user].accessible_repairs.find(args[:id])
        }
      )
    end

    field :users do
      type types[!UserType]
      description 'List all Users'

      resolve GraphqlTypes.compose_resolve(
        :must_auth,
        ->(obj, args, ctx){
          if ctx[:user].manager?
            User.where(nil)
          elsif ctx[:user].user?
            [ctx[:user]]
          else
            raise StandardError.new('Unknown user type')
          end
        }
      )
    end

    field :user do
      type UserType
      argument :id, !types.ID
      description 'Find a User by ID'

      resolve GraphqlTypes.compose_resolve(
        :must_auth,
        ->(obj, args, ctx){
          if ctx[:user].manager?
            User.find(args[:id])
          else
            nil
          end
        }
      )
    end

    field :auth_token do
      type AuthToken
      description 'Authentication token'

      argument :email, !types.String
      argument :password, !types.String

      resolve ->(obj, args, ctx){
        User.find_by(email: args[:email])&.authenticate(args[:password]) || nil
      }
    end
  end

  AuthToken = def_type do
    name 'authentication_token'
    description 'User/Manager auth token'

    field :jwt, !types.String
    field :can_see_users, !types.Boolean
    field :can_update_repairs, !types.Boolean
  end
  
  RepairType = def_type do
    name 'repair'
    description 'Auto shop repair'

    field :id, !types.ID
    field :assignee, UserType
    field :description, !types.String
    field :comments, types[!RepairCommentType]
    field :time, !TimeType
    field :status, !types.String
    field :created_at, !TimeType
  end

  RepairCommentType = def_type do
    name 'repair_comment'
    
    field :id, !types.ID
    field :repair_id, !types.ID
    field :text, !types.String
    field :created_at, !TimeType
    field :author, !UserType
  end

  UserType = def_type do
    name 'user'

    field :id, !types.ID
    field :email, !types.String
    field :name, !types.String
    field :role, !types.String
  end

end
