class DeleteUserAction < ApplicationAction

  requires_auth 'manager'

  def perform
    user = User.find(args[:id])
    user.destroy!
    user
  end

end