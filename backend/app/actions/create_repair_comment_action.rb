class CreateRepairCommentAction < ApplicationAction

  requires_auth

  def perform
    data = args.merge(author: ctx[:user])
    Repair.find(args[:repair_id]).comments.create!(data)
  end

end