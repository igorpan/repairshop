class CreateRepairAction < ApplicationAction

  requires_auth 'manager'

  def perform
    Repair.create!(args)
  end

end