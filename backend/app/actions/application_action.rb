class ApplicationAction

  AuthError = Class.new(StandardError)

  class << self
    def requires_auth(user_role=nil)
      @before_actions ||= []
      @before_actions << ->(args, ctx){
        if user_role.nil?
          raise AuthError.new('You must be logged in to perform this action') unless ctx[:user].present?
        else
          raise AuthError.new('You are not allowed to perform this action') unless ctx[:user]&.role == user_role
        end
      }
    end


  end
  
  def initialize(args, ctx)
    @args = args
    @ctx = ctx
  end

  def call
    (self.class.instance_variable_get(:@before_actions) || []).each do |action|
      action.call(args, ctx)
    end
    perform()
  end

private

  attr_reader :args, :ctx

end