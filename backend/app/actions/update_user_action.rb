class UpdateUserAction < ApplicationAction
  
    requires_auth 'manager'
  
    def perform
      user = User.find(args[:id])
      user.update!(args)
      user
    end
  
  end