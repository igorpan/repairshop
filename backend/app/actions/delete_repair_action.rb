class DeleteRepairAction < ApplicationAction

  requires_auth 'manager'
  
    def perform
      repair = Repair.find(args[:id])
      repair.destroy!
      repair
    end

end