class UpdateRepairAction < ApplicationAction

  requires_auth

  def perform
    repair = Repair.find(args[:id])
    if ctx[:user].manager?
      data = args
    elsif repair.status == 'incomplete' && args[:status] == 'complete'
      data = args.slice(:status)
    else
      data = {}
    end
    repair.update!(data)
    repair
  end

end