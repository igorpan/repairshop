class CreateUserAction < ApplicationAction
  
    requires_auth 'manager'
  
    def perform
      User.create!(args)
    end
  
  end