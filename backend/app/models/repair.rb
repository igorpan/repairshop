class Repair < ApplicationRecord

  belongs_to :assignee, class_name: 'User', required: false
  has_many :comments, dependent: :destroy

  validates :time, :status, :description, presence: true
  validates :status, inclusion: { in: ['complete', 'incomplete', 'approved'] }
  validate :time_availability

  after_initialize{
    self.time ||= Time.current
    self.status ||= 'incomplete'
    self.description ||= ''
  }

  before_save{
    self[:end_time] = end_time
  }

private

  attr_writer :end_time

  def end_time
    time + 1.hour
  end

  def time_availability
    if overlapping_repair.present? && overlapping_repair != self
      self.errors.add(
        :time, 
        :time_availability, 
        message: 'overlaps with another repair'
      )
    end
  end

  def overlapping_repair
    self.class.where(
      '(time >= ? AND time < ?) OR (end_time > ? AND end_time < ?)',
      time,
      end_time,
      time,
      end_time
    ).first
  end

end
