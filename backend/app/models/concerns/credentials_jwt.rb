module CredentialsJWT
  extend ActiveSupport::Concern

  ALGORITHM = 'HS256'
  SECRET = Rails.application.secrets.jwt_secret
  TOKEN_EXPIRATION_LEEWAY = 60 # seconds

  TokenError = Class.new(StandardError)
  CredentialsInvalidError = Class.new(TokenError)
  TokenInvalidError = Class.new(CredentialsInvalidError)
  TokenExpiredError = Class.new(CredentialsInvalidError)
  NotFoundError = Class.new(CredentialsInvalidError)
  TokenGenerationError = Class.new(TokenError)

  class_methods do
    def for_jwt(jwt)
      begin
        payload = JWT.decode(jwt, SECRET, true, { algorithm: ALGORITHM, exp_leeway: TOKEN_EXPIRATION_LEEWAY })[0]
      rescue JWT::ExpiredSignature
        raise TokenExpiredError.new('JWT has expired')
      rescue JWT::VerificationError, JWT::DecodeError
        raise TokenInvalidError.new('JWT could not be verified')
      end
      data = payload['data']
      credentials = find_by(id: data['credentials_id'])
      if credentials.nil?
        raise NotFoundError.new('Credentials for given JWT were not found. User might have been deleted.')
      else
        credentials
      end
    end
  end

  def jwt
    raise TokenGenerationError.new('Credentials must have id in order for JWT to be generated') if id.nil?
    JWT.encode(
      { 
        'data' => { 'credentials_id' => id },
        'exp' => 7.days.from_now.to_i
      }, 
      SECRET, 
      ALGORITHM
    )
  end
end