class User < ApplicationRecord

  include CredentialsJWT
  has_secure_password

  has_many :repairs, foreign_key: :assignee_id, dependent: :nullify
  has_many :comments, foreign_key: :author_id, dependent: :destroy

  validates :name, :email, presence: true
  validates :email, uniqueness: true
  validates :role, inclusion: { in: ['manager', 'user'] }
  
  after_initialize{
    self.name ||= ''
  }

  alias_method :accessible_repairs, :repairs

  def accessible_repairs
    if manager?
      Repair.where(nil)
    else
      repairs
    end
  end

  def manager?
    role == 'manager'
  end

  def user?
    role == 'user'
  end

  def can_see_users
    manager?
  end

  def can_update_repairs
    manager?
  end

end
