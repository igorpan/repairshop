class Comment < ApplicationRecord

  belongs_to :repair
  belongs_to :author, class_name: 'User'
  
end
