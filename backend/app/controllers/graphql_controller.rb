class GraphqlController < ApplicationController
  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: { errors: { full_messages: e.record.errors.full_messages } }, status: :bad_request
  end
  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: { error: 'Not found' }, status: :not_found
  end

  def execute
    variables = ensure_hash(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    context = {
      user: authenticated_user
    }
    result = GraphqlSchema.execute(query, variables: variables, context: context, operation_name: operation_name)
    render json: result
  end

private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end
end
