class ApplicationController < ActionController::API
  rescue_from CredentialsJWT::CredentialsInvalidError, ApplicationAction::AuthError do |e|
    render json: { 'error' => 'Unauthorized' }, status: 401
  end

private 
  
  def authenticated_user
    auth_header = request.headers['HTTP_AUTHORIZATION'] || ''
    auth_scheme, jwt = auth_header.split(' ', 2)
    if auth_scheme == 'Bearer' && jwt.present?
      begin
        credentials = User.for_jwt(jwt)
      rescue CredentialsJWT::TokenError
        nil
      end
    else
      nil
    end
  end
end
