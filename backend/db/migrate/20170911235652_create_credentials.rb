class CreateCredentials < ActiveRecord::Migration[5.1]
  def change
    create_table :credentials do |t|
      t.string :email
      t.string :password_digest

      t.timestamps
    end
  end
end
