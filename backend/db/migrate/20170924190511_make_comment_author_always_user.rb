class MakeCommentAuthorAlwaysUser < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :author_id
    remove_column :comments, :author_type
    add_reference :comments, :author, foreign_key: { to_table: :users }
  end
end
