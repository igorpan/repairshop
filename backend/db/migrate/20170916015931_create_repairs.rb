class CreateRepairs < ActiveRecord::Migration[5.1]
  def change
    create_table :repairs do |t|
      t.datetime :time, null: false
      t.string :description, null: false
      t.string :status, null: false

      t.timestamps
    end
  end
end
