class AddEndTimeToRepairs < ActiveRecord::Migration[5.1]
  def change
    add_column :repairs, :end_time, :datetime, null: false
  end
end
