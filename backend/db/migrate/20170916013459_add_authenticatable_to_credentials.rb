class AddAuthenticatableToCredentials < ActiveRecord::Migration[5.1]
  def change
    change_table :credentials do |t|
      t.references :authenticatable, polymorphic: true, index: { name: 'index_credentials_on_authenticatable' }
    end
  end
end
