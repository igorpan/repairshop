class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :text
      t.references :repair, foreign_key: true
      t.references :author, polymorphic: true

      t.timestamps
    end
  end
end
