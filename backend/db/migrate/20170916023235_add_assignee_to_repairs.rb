class AddAssigneeToRepairs < ActiveRecord::Migration[5.1]
  def change
    add_reference :repairs, :assignee, foreign_key: { to_table: :users }
  end
end
