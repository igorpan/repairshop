class MakeCredentialsAuthenticatableAlwaysUser < ActiveRecord::Migration[5.1]
  def change
    remove_column :credentials, :authenticatable_id
    remove_column :credentials, :authenticatable_type
    add_reference :credentials, :authenticatable, foreign_key: { to_table: :users }
  end
end
