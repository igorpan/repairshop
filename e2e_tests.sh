#!/bin/bash
rm backend/tmp/pids/server.pid
(cd backend && RAILS_ENV=test bundle exec rake db:test:load)
(cd backend && RAILS_ENV=test rails s &)
(cd frontend && yarn run nightwatch)