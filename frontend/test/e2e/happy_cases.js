var config = require('../../nightwatch.conf.js');

const ROOT_URL = 'http://localhost:3001';

function loginUsing(client, email, password) {
  return client
    .url(ROOT_URL)
    .waitForElementVisible('body', 1000)
    .assert.visible('form input[type=email]')
    .setValue('form input[type=email]', email)
    .setValue('form input[type=password]', password)
    .click('form button')
    .waitForElementVisible('a[href="/logout"]', 500);
}

function loginAsManager(client) {
  return loginUsing(client, 'manager@test.com', 'test');
}

module.exports = {
  'Manager login': function (client) {
    loginAsManager(client)
      .assert.elementPresent('a[href="/repairs"]')
      .assert.elementPresent('a[href="/users"]')
      .end();
  },

  'Create and delete repair': function (client) {
    loginAsManager(client)
      .click('#new_repair')
      .waitForElementVisible('#rep_save')
      .setValue('#rep_description', 'My Description')
      .click('#rep_time').click('#rep_time [aria-label=day-26]')
      .click('#rep_assignee').click('#rep_assignee .Dropdown-option:last-child')
      .click('#rep_save')
      .waitForElementVisible('#new_repair')
      .assert.containsText('table td', 'My Description')
      .click('table tr:first-child td:last-child button')
      .waitForElementVisible('#rep_save')
      .click('#rep_delete')
      .waitForElementVisible('#new_repair')
      .end();
  },

  'Create user': function (client) {
    loginAsManager(client)
      .click('a[href="/users"]')
      .waitForElementVisible('#new_user')
      .click('#new_user')
      .setValue('#usr_name', 'Log me in')
      .setValue('#usr_email', 'logmein@test.com')
      .setValue('#usr_pass', 'logmein')
      .click('#usr_save')
      .waitForElementVisible('#new_user')
      .assert.containsText('table tbody tr:last-child td:nth-child(2)', 'Log me in')
      .end();
  },

  'Login using manually created user': function (client) {
    loginUsing(client, 'logmein@test.com', 'logmein');
  },
};