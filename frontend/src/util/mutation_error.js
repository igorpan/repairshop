const mutationError = (error) => {
  let errorsForUser = [];
  let isValidationError = false;
  if (error.graphQLErrors && error.graphQLErrors.full_messages) {
    errorsForUser = error.graphQLErrors.full_messages;
    isValidationError = true;
  } else {
    console.error(error);
    errorsForUser = ['Oops, that\'s an unexpected error. We failed miserably :-('];
  }
  return {
    errorsForUser,
    isValidationError,
  };
};

export default mutationError;