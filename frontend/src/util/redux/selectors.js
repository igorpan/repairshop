import { selectors as authSelectors } from './auth';
import { selectors as repairsSelectors } from './repairs';

const getAuthToken = (state) => authSelectors.getAuthToken(state.auth);
const getRepairsFilter = (state) => repairsSelectors.getRepairsFilter(state.repairs);

export {
  getAuthToken,
  getRepairsFilter,
};