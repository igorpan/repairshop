import { 
  createStore as creteReduxStore, 
  combineReducers, 
  applyMiddleware, 
  compose,
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { reducer as authReducer } from './auth';
import { reducer as repairsReducer } from './repairs';

import credentialsStore from 'util/credentials_store';
import { login } from './actions';

const afterCreate = store => {
  const authToken = credentialsStore.fetch();;
  if (authToken) {
    store.dispatch(login(authToken));
  }
};

const createStore = (apolloClient, history) => {
  const store = creteReduxStore(
    combineReducers({
      apollo: apolloClient.reducer(),
      auth: authReducer,
      router: routerReducer,
      repairs: repairsReducer,
    }),
    {},
    compose(
      applyMiddleware(
        thunkMiddleware,
        apolloClient.middleware(),
        routerMiddleware(history)
      ),
      (typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined') ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f,
    )
  );
  afterCreate(store);
  return store;
};

export default createStore;