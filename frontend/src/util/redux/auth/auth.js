import credentialsStore from 'util/credentials_store';

const LOGIN = 'AUTH_LOGIN';
const LOGOUT = 'AUTH_LOGOUT';



// REDUCER

const reducer = (
  state = {
    token: null
  },
  action
) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, token: action.token  };
    case LOGOUT:
      return { ...state, token: null };
    default:
      return state;
  }
};



// ACTIONS

const login = (token) => {
  credentialsStore.store(token);
  return {
    type: LOGIN,
    token: token,
  };
};

const logout = () => {
  credentialsStore.remove();
  return {
    type: LOGOUT,
  };
};

const actions = {
  login,
  logout,
};



// SELECTORS

const getAuthToken = (state) => state.token;

const selectors = {
  getAuthToken,
};



export { actions, selectors, reducer };