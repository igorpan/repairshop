import moment from 'moment'

const CHANGE_FILTER = 'CHANGE_REPAIRS_FILTER';

// Reducer

const reducer = (
  state = {
    filter: {
      from_time: moment().subtract(3, 'months').startOf('day'),
      to_time: moment().add(3, 'months').endOf('day'),
      assignee_id: null,
      status: null,
    }
  },
  action
) => {
  switch (action.type) {
    case CHANGE_FILTER:
      return { ...state, filter: action.filter };
    default:
      return state;
  }
}



// Actions

const actions = {
  changeRepairsFilter: filter => ({
    type: CHANGE_FILTER,
    filter,
  }),
};



// Selectors

const getRepairsFilter = state => state.filter;

const selectors = {
  getRepairsFilter,
};



export {
  selectors,
  actions,
  reducer,
};