import { actions as authActions } from './auth';
import { actions as repairActions } from './repairs';
import { push } from 'react-router-redux';

const login = authActions.login;
const logout = authActions.logout;
const changeRepairsFilter = repairActions.changeRepairsFilter;

const openApp = () => push('/repairs');
const openUserEditor = (id) => push(`/users/${id || 'new'}`);
const openUsers = () => push(`/users`);
const openRepairs = () => push(`/repairs`);
const openRepairEditor = (id) => push(`/repairs/${id || 'new'}`);

export {
  login,
  logout,
  changeRepairsFilter,
  openApp,
  openUserEditor,
  openUsers,
  openRepairs,
  openRepairEditor,
};