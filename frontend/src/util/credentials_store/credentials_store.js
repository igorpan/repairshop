const CREDS_KEY = 'credentials';

const credentialsStore = {
  store: (credentials) => {
    localStorage.setItem(CREDS_KEY, JSON.stringify(credentials));
  },
  fetch: () => {
    const jsonCreds = localStorage.getItem(CREDS_KEY);
    if (jsonCreds) {
      return JSON.parse(jsonCreds);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(CREDS_KEY);
  }
};

export default credentialsStore;