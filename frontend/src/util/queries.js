import { gql } from 'react-apollo';

const UpdateUserQuery = gql`
  mutation update_user($id: ID!, $name: String, $email: String, $role: String, $password: String){
    update_user(id: $id, email: $email, password: $password, role: $role, name: $name){
      id
      name
      email
      role
    }
  }
`;

const CreateUserQuery = gql`
  mutation create_user($name: String!, $email: String!, $role: String!, $password: String!){
    create_user(email: $email, password: $password, role: $role, name: $name){
      id
      name
      email
      role
    }
  }
`;

const UserQuery = gql`
  query user($id: ID!){
    user(id: $id){
      id
      name
      email
      role
    }
  }
`;

const UserDeleteQuery = gql`
  mutation delete_user($id: ID!){
    delete_user(id: $id){
      id
    }
  }
`;

const UsersQuery = gql`
  query users{
    users{
      id
      name
      role
    }
  }
`;

const RepairQuery = gql`
  query repair($id: ID!){
    repair(id: $id){
      id
      description
      time
      status
      assignee{
        id
      }
    }
  }
`;

const CreateRepairQuery = gql`
  mutation create_repair($time: Time!, $assignee_id: ID, $description: String!, $status: String!){
    create_repair(time: $time, assignee_id: $assignee_id, description: $description, status: $status){
      id
      time
      description
      status
      assignee{
        id
      }
    }
  }
`;

const UpdateRepairQuery = gql`
  mutation update_repair($id: ID!, $time: Time, $assignee_id: ID, $description: String, $status: String){
    update_repair(id: $id, time: $time, assignee_id: $assignee_id, description: $description, status: $status){
      id
      time
      description
      status
      assignee{
        id
      }
    }
  }
`;

const RepairsQuery = gql`
  query repairs($from_time: Time!, $to_time: Time!, $assignee_id: ID, $status: String){
    repairs(from_time: $from_time, to_time: $to_time, assignee_id: $assignee_id, status: $status){
      id
      description
      time
      status
      assignee{
        id
      }
    }

    users{
      id,
      name
    }
  }
`;

const RepairDeleteQuery = gql`
  mutation delete_repair($id: ID!){
    delete_repair(id: $id){
      id
    }
  }
`;

const RepairCommentsQuery = gql`
  query repair_comments($repair_id: ID!){
    repair(id: $repair_id){
      id
      comments{
        id
        text
        author{
          name
        }
      }
    }
  }
`;

const CreateRepairCommentQuery = gql`
  mutation create_repair_comment($text: String!, $repair_id: ID!){
    create_repair_comment(text: $text, repair_id: $repair_id) {
      id
    }
  }
`;

export {
  UpdateUserQuery,
  CreateUserQuery,
  UserQuery,
  UserDeleteQuery,
  UsersQuery,
  RepairQuery,
  CreateRepairQuery,
  UpdateRepairQuery,
  RepairsQuery,
  RepairDeleteQuery,
  RepairCommentsQuery,
  CreateRepairCommentQuery,
};