import React from 'react';
import s from './Table.scss';

const Table = ({ columns, rows }) => (
  <div className={s.wrapper}>
    <div className={s.tableBorder}>
      <table>
        <thead>
          <tr>
            {columns.map(column => (
              <th style={{ width: column.width || 'auto' }} key={column.label}>{column.label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, i) => (
            <tr key={i}>
              {row.map((cell, j) => (
                <td key={j}>{typeof cell === 'function' ? cell() : cell}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
);

export default Table;