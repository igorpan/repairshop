import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getAuthToken } from 'util/redux/selectors';

let AuthenticatedOnly = ({ children, token }) => {
  if (token) {
    return <Redirect to={'/repairs'} />;
  } else {
    return children;
  }
};

const mapStateToProps = state => ({
  token: getAuthToken(state),
});

AuthenticatedOnly = connect(mapStateToProps, null)(AuthenticatedOnly);

export default AuthenticatedOnly;