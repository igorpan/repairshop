import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getAuthToken } from 'util/redux/selectors';
import { withApollo } from 'react-apollo';

let AuthenticatedOnly = ({ children, token, client }) => {
  if (!token) {
    client.resetStore();
    return <Redirect to={'/login'} />;
  } else {
    return children;
  }
};

const mapStateToProps = state => ({
  token: getAuthToken(state),
});

AuthenticatedOnly = connect(mapStateToProps, null)(AuthenticatedOnly);
AuthenticatedOnly = withApollo(AuthenticatedOnly);

export default AuthenticatedOnly;