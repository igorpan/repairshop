import React from 'react';
import { SyncLoader } from 'react-spinners';
import s from './SpinnerOrRender.scss';

const SpinnerOrRender = ({ loading, render }) => {
  if (loading) {
    return (
      <div className={s.spinnerWrapper}>
        <SyncLoader size={15} color={'#4FA0FF'} loading={true} />
      </div>
    )
  } else {
    return render();
  }
};

export default SpinnerOrRender;