import React from 'react';
import Dropdown from 'react-dropdown';
import classNames from 'classnames';
import s from './Select.scss';

const Select = ({ options, onChange, value, placeholder, fillWidth, disabled, id }) => (
  <div id={id}>
    <Dropdown 
      options={options.map(o => ({ ...o, value: o.value === null ? '' : o.value }))}
      onChange={selected => {
        if (selected.label === selected.value && options.find(o => o.label === selected.label).value === null) {
          onChange(null);
        } else {
          onChange(selected.value);
        }
      }}
      value={options.find(o => o.value === value)}
      placeholder={placeholder}
      className={classNames(s.wrapper, { [s.fill]: fillWidth, [s.disabled]: disabled })}
    />
  </div>
);

export default Select;