import React from 'react';
import s from './ErrorList.scss';

const ErrorList = ({ errors }) => {
  if (errors.length === 0) {
    return null;
  }
  return (
    <div>
      <ul className={s.list}>
        {errors.map((error, i) => (
          <li key={i}>{error}</li>
        ))}
      </ul>
    </div>
  );
};

export default ErrorList;