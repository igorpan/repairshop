import React from 'react';
import ReactDatePicker from 'react-datepicker';
import TextInput from 'Form/TextInput';
import classNames from 'classnames';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import s from './DateTimePicker.scss';

const DateTimePickerInput = ({ onClick, value }) => (
  <div className={s.input}>
    <TextInput
      onChange={() => {}}
      value={value}
      onClick={onClick}
    />
  </div>
);

const DateTimePicker = ({ 
  value, 
  onChange,
  selectsStart,
  selectsEnd,
  startDate,
  endDate,
  disabled,
  id
}) => (
  <div id={id} className={classNames(s.wrapper, { [s.disabled]: disabled })}>
    <ReactDatePicker
      customInput={<DateTimePickerInput />}
      selected={value}
      onChange={onChange}
      showTimeSelect={true}
      calendarClassName={s.calendarWrapper}
      selectsStart={selectsStart}
      selectsEnd={selectsEnd}
      startDate={startDate}
      endDate={endDate}
      dateFormat={'MMM D, YY hh:mmA'}
    />
  </div>
);

export default DateTimePicker;