import React from 'react';
import { Form as RoForm } from 'ro-form';

const Form = (props) => (
  <RoForm {...props} />
);

export default Form;