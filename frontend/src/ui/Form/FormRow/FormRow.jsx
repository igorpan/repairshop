import React from 'react';
import s from './FormRow.scss';

const FormRow = ({ label, children }) => (
  <div className={s.wrapper}>
    <div className={s.label}>{label}</div>
    <div>{children}</div>
  </div>
);

export default FormRow;