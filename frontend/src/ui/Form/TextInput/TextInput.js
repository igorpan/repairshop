import React from 'react';
import classNames from 'classnames';
import s from './TextInput.scss';

const TextInput = ({ type, placeholder, value, onChange, onClick, required, onKeyDown, disabled, id }) => (
  <input
    id={id}
    type={type || 'text'}
    placeholder={placeholder}
    required={!!required}
    value={value}
    onChange={e => onChange(e.target.value)}
    onClick={onClick}
    className={classNames(s.input, { [s.disabled]: disabled })}
    onKeyDown={onKeyDown}
  />
);

export default TextInput;