import React from 'react';
import s from './PageHeader.scss';

const PageHeader = ({ title, actionContent }) => (
  <div className={s.wrapper}>
    <div>
      {title}
    </div>
    <div className={s.actionContent}>
      {actionContent}
    </div>
  </div>
);

export default PageHeader;