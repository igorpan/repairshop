import React from 'react';
import s from './InputCell.scss';

const InputCell = ({ label, inputNode }) => (
  <div className={s.wrapper}>
    <label>{label}</label>
    <div>
      {inputNode}
    </div>
  </div>
);

export default InputCell;