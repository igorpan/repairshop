import React from 'react';
import s from './Toolbar.scss';

import InputCell from './InputCell';

const Toolbar = ({ children }) => (
  <div className={s.wrapper}>{children}</div>
);

export default Toolbar;
export {
  InputCell as ToolbarInputCell
};