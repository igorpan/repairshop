import React from 'react';
import classNames from 'classnames';
import s from './Button.scss';

const Button = ({ id, children, onClick, type, small, dangerous }) => (
  <button 
    id={id}
    onClick={onClick} 
    type={type || 'button'}
    className={classNames(s.button, { [s.small]: small, [s.dangerous]: dangerous })}
  >
    {children}
  </button>
);

export default Button;