import React from 'react';
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import {
  ConnectedRouter,
} from 'react-router-redux';
import { ApolloClient, ApolloProvider, createNetworkInterface } from 'react-apollo';
import createStore from 'util/redux/create_store';
import createHistory from 'history/createBrowserHistory';
import { getAuthToken } from 'util/redux/selectors';

import { AuthenticatedOnly, UnauthenticatedOnly } from 'AuthRedirect';
import InAppLayout from './routes/InApp/Layout';

import Login from './routes/Login';
import Logout from './routes/InApp/Logout';
import Repairs from './routes/InApp/Repairs/Repairs';
import Repair from './routes/InApp/Repairs/Repair';
import Users from './routes/InApp/Users/Users';
import User from './routes/InApp/Users/User';

import './reset.scss';

const history = createHistory();

const networkInterface = createNetworkInterface({
  uri: window.SERVER_ROOT + '/graphql',
});

const client = new ApolloClient({
  networkInterface: networkInterface,
});

const store = createStore(client, history);

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};
    }
    const token = getAuthToken(store.getState());
    if (token) {
      req.options.headers.authorization = `Bearer ${token.jwt}`;
    }
    next();
  }
}]);

export default () => (
  <ApolloProvider store={store} client={client}>
    <ConnectedRouter history={history}>
      <div>
        <Switch>
          <Route
            exact
            path={'/'}
            render={props => (
              <Redirect to={'/login'} />
            )}
          />

          <Route 
            exact 
            path={'/login'} 
            render={props => (
              <UnauthenticatedOnly>
                <Login />
              </UnauthenticatedOnly>
            )}
          />

          <Route
            exact
            path={'/logout'}
            component={Logout}
          />

          <Route
            path={'/'}
            render={props => (
              <AuthenticatedOnly>
                <InAppLayout>
                  <Route exact path={'/repairs'} component={Repairs} />
                  <Route exact path={'/repairs/:id'} component={Repair} />
                  <Route exact path={'/users'} component={Users} />
                  <Route exact path={'/users/:id'} component={User} />
                </InAppLayout>
              </AuthenticatedOnly>
            )}
          />
        </Switch>
      </div>
    </ConnectedRouter>
  </ApolloProvider>
);