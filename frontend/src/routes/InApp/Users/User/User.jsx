import React, { Component } from 'react';
import PageHeader from 'PageHeader';
import { Form, FormControl, TextInput, Select, FormRow, ErrorList } from 'Form';
import Button from 'Button';
import { SpinnerOrRender } from 'Spinner';
import s from './User.scss';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import { openUsers } from 'util/redux/actions';
import { 
  UpdateUserQuery,
  CreateUserQuery,
  UserQuery,
  UserDeleteQuery,
  UsersQuery,
} from 'util/queries';
import mutationError from 'util/mutation_error';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: '',
        email: '',
        role: 'user',
        password: '',
      },
      errors: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data.user !== this.state.formData) {
      this.setState({ formData: { ...nextProps.data.user, password: '' } });
    }
  }

  _changeFormData = newFormData => {
    this.setState({ formData: newFormData });
  };

  _isNew = () => !this.props.data;

  _save = () => {
    const { data, updateUser, createUser, openUsers } = this.props;
    const { formData } = this.state;
    let promise;
    if (this._isNew()) {
      promise = createUser({ 
        variables: formData,
        refetchQueries: [{ query: UsersQuery }],
      });
    } else {
      promise = updateUser({
        variables: { id: data.user.id, ...formData },
      });
    }
    promise
      .then(() => {
        openUsers();
      })
      .catch(e => this.setState({ errors: mutationError(e).errorsForUser }));
  };

  _destroy = () => {
    const { deleteUser, data, openUsers } = this.props;
    deleteUser({
      variables: { id: data.user.id },
      refetchQueries: [{ query: UsersQuery }],
    }).then(() => {
      openUsers();
    });
  };

  _networkErrors = () => {
    const { data } = this.props;
    return data && data.error && data.error.networkError ? ['Could not fetch requested user'] : [];
  };

  _errors = () => {
    const { errors } = this.state;
    return [
      ...errors,
      ...this._networkErrors(),
    ];
  };

  render() {
    const { data } = this.props;
    const { formData } = this.state;

    return (
      <div>
        <PageHeader 
          title={'Edit User'}
          actionContent={
            <div>
              <Button id={'usr_save'} onClick={this._save}>Save</Button>
            </div>
          }
        />
        <SpinnerOrRender 
          loading={data && data.loading}
          render={() => {
            let formNode, deleteButtonNode;

            if (this._networkErrors().length === 0) {
              formNode = (
                <Form value={formData} onChange={this._changeFormData} onSubmit={this._save}>
                  <FormRow label={'Name*'}>
                    <FormControl 
                      control={TextInput}
                      controlProps={{ id: 'usr_name', required: true }}
                      path={'name'}
                    />
                  </FormRow>
                  <FormRow label={'Email*'}>
                    <FormControl 
                      control={TextInput}
                      controlProps={{ id: 'usr_email', required: true, type: 'email' }}
                      path={'email'}
                    />
                  </FormRow>
                  <FormRow label={'Role*'}>
                    <FormControl
                      control={Select}
                      controlProps={{ 
                        id: 'usr_role',
                        options: [
                          { value: 'manager', label: 'Manager' }, 
                          { value: 'user', label: 'User' }
                        ],
                        fillWidth: true,
                      }}
                      path={'role'}
                    />
                  </FormRow>
                  <FormRow label={'Password' + (this._isNew() ? '*' : '')}>
                    <FormControl 
                      control={TextInput}
                      controlProps={{ id: 'usr_pass', required: true, placeholder: 'Leave blank to keep the old one', type: 'password' }}
                      path={'password'}
                    />
                  </FormRow>
                </Form>
              );

              if (!this._isNew()) {
                deleteButtonNode = (
                  <div className={s.deleteWrapper}>
                    <Button id={'usr_delete'} onClick={this._destroy} small dangerous>Delete user !!!1!</Button>
                  </div>
                );
              }
            }

            return (
              <div>
                <div className={s.formWrapper}>
                  <ErrorList errors={this._errors()} />
                  {formNode}
                  {deleteButtonNode}
                </div>
              </div>
            );
          }}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  openUsers: () => dispatch(openUsers()),
});

User = compose(
  graphql(UserQuery, {
    skip: ({ match }) => match.params.id === 'new',
    options: ({ match }) => ({
      variables: { id: match.params.id },
    })
  }),
  graphql(UpdateUserQuery, { name: 'updateUser' }),
  graphql(CreateUserQuery, { name: 'createUser' }),
  graphql(UserDeleteQuery, { name: 'deleteUser' }),
  connect(null, mapDispatchToProps)
)(User);

export default User;