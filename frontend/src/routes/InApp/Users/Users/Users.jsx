import React, { Component } from 'react';
import PageHeader from 'PageHeader';
import Table from 'Table';
import Button from 'Button';
import { SpinnerOrRender } from 'Spinner';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import { openUserEditor } from 'util/redux/actions';
import s from './Users.scss';
import { UsersQuery } from 'util/queries';

class Users extends Component {
  render() {
    const { data, openUserEditor } = this.props;

    return (
      <div>
        <PageHeader 
          title={'Users'}
          actionContent={
            <div>
              <Button id={'new_user'} onClick={() => openUserEditor()}>New User</Button>
            </div>
          }
        />
        <SpinnerOrRender
          loading={data.loading}
          render={() => (
            <Table 
              columns={[
                {
                  label: 'ID',
                  width: '10%'
                },
                {
                  label: 'Name',
                  width: '20%',
                },
                {
                  label: 'Role',
                  width: '30%',
                },
                {
                  label: '',
                  width: '40%',
                }
              ]}
              rows={data.users.map(user => [
                user.id,
                user.name,
                user.role,
                () => (
                  <div className={s.actions}>
                    <Button small onClick={() => openUserEditor(user.id)}>Edit</Button>
                  </div>
                )
              ])}
            />
          )}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  openUserEditor: (id) => dispatch(openUserEditor(id)),
})

Users = compose(
  graphql(UsersQuery),
  connect(null, mapDispatchToProps)
)(Users);

export default Users;