import React from 'react';
import { Redirect } from 'react-router-dom';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import { logout } from 'util/redux/actions';

let Logout = ({ client, dispatch }) => {
  client.resetStore();
  dispatch(logout());

  return <Redirect to={'/login'} />;
};

Logout = withApollo(connect()(Logout));

export default Logout;