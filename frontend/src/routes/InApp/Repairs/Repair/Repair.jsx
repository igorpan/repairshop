import React, { Component } from 'react';
import PageHeader from 'PageHeader';
import { Form, FormControl, TextInput, Select, FormRow, ErrorList, DateTimePicker } from 'Form';
import Button from 'Button';
import Comments from './Comments';
import { SpinnerOrRender } from 'Spinner';
import moment from 'moment';
import s from './Repair.scss';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import { openRepairs } from 'util/redux/actions';
import { getRepairsFilter, getAuthToken } from 'util/redux/selectors';
import {
  UsersQuery,
  RepairQuery,
  CreateRepairQuery,
  UpdateRepairQuery,
  RepairsQuery,
  RepairDeleteQuery,
} from 'util/queries';
import mutationError from 'util/mutation_error';

class Repair extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        description: '',
        time: moment(),
        status: 'incomplete',
        assignee_id: null,
      },
      errors: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    this._repairDataToState(nextProps);
  }

  componentWillMount() {
    this._repairDataToState(this.props);
  }

  _repairDataToState = props => {
    if (
      props.repairData && 
      !props.repairData.loading && 
      !props.repairData.error &&
      props.repairData.repair.id !== this.state.formData.id
    ) {
      const assignee = props.repairData.repair.assignee;
      this.setState({
        formData: {
          ...props.repairData.repair,
          assignee_id: assignee ? assignee.id : null,
          time: moment(props.repairData.repair.time),
        } 
      });
    }
  };

  _changeFormData = newData => {
    this.setState({ formData: newData });
  };

  _isNew = () => !this.props.repairData;

  _isLoading = () => {
    const { repairData } = this.props;
    return repairData && repairData.loading;
  };

  _wasIncompleteWhenLoaded = () => {
    const { repairData } = this.props;
    return !this._isNew() && !this._isLoading() && repairData.repair.status === 'incomplete';
  };

  _save = () => {
    const { createRepair, updateRepair, openRepairs, repairsFilter } = this.props;
    const { formData } = this.state;
    let promise;
    if (this._isNew()) {
      promise = createRepair({
        variables: formData,
        refetchQueries: [{
          query: RepairsQuery,
          variables: repairsFilter,
        }]
      });
    } else {
      promise = updateRepair({
        variables: formData,
      });
    }
    promise
      .then(() => {
        openRepairs();
      })
      .catch(e => {
        this.setState({ errors: mutationError(e).errorsForUser })
      });
  };

  _destroy = () => {
    const { deleteRepair, repairData, openRepairs, repairsFilter } = this.props;
    deleteRepair({
      variables: { id: repairData.repair.id },
      refetchQueries: [{
        query: RepairsQuery,
        variables: repairsFilter,
      }]
    }).then(() => {
      openRepairs();
    });
  };

  _availableStatusOptions = () => {
    const { authToken } = this.props;

    if (this._isLoading()) {
      return [];
    }

    let basicOptions = [
      { value: 'incomplete', label: 'Incomplete' },
      { value: 'complete', label: 'Complete' },
    ];

    if (!authToken.can_update_repairs && this._wasIncompleteWhenLoaded()) {
      return basicOptions;
    } else {
      return [
        ...basicOptions,
        { value: 'approved', label: 'Complete&Approved' },
      ];
    }
  }

  _networkErrors = () => {
    const { repairData } = this.props;
    return repairData && repairData.error && repairData.error.networkError ? ['Could not fetch requested repair'] : [];
  };

  _errors = () => {
    const { errors } = this.state;
    return [
      ...errors,
      ...this._networkErrors(),
    ];
  };

  render() {
    const { formData, errors } = this.state;
    const { usersData, match, authToken, repairData } = this.props;

    let commentsNode;
    if (!this._isNew()) {
      commentsNode = (
        <FormRow label={'Comments'}>
          <Comments 
            repairId={match.params.id}
          />
        </FormRow>
      );
    }

    return (
      <div>
        <PageHeader
          title={'Edit Repair'}
          actionContent={
            <div>
              <Button id={'rep_save'} onClick={this._save}>
                Save
              </Button>
            </div>
          }
        />
        <div className={s.wrapper}>
          <div className={s.formSide}>
            <SpinnerOrRender
              loading={repairData && repairData.loading}
              render={() => {
                let formNode, deleteButtonNode;

                if (this._networkErrors().length === 0) {
                  formNode = (
                    <Form value={formData} onChange={this._changeFormData} onSubmit={this._save}>
                      <FormRow label={'Description'}>
                        <FormControl
                          control={TextInput}
                          controlProps={{ id: 'rep_description', disabled: !authToken.can_update_repairs }}
                          path={'description'}
                        />
                      </FormRow>
                      <FormRow label={'Time'}>
                        <FormControl
                          control={DateTimePicker}
                          controlProps={{ 
                            id: 'rep_time',
                            fillWidth: true, 
                            disabled: !authToken.can_update_repairs 
                          }}
                          path={'time'}
                        />
                      </FormRow>
                      <FormRow label={'Assignee'}>
                        <FormControl
                          control={Select}
                          controlProps={{
                            id: 'rep_assignee',
                            fillWidth: true,
                            disabled: !authToken.can_update_repairs,
                            options: usersData.loading ? [] : [
                              { value: null, label: 'Nobody' },
                              ...usersData.users.map(user => ({
                                value: user.id,
                                label: user.name,
                              }))
                            ]
                          }}
                          path={'assignee_id'}
                        />
                      </FormRow>
                      <FormRow label={'Status'}>
                        <FormControl
                          control={Select}
                          controlProps={{
                            id: 'rep_status',
                            fillWidth: true,
                            disabled: !authToken.can_update_repairs && !this._wasIncompleteWhenLoaded(),
                            options: this._availableStatusOptions(),
                          }}
                          path={'status'}
                        />
                      </FormRow>
                    </Form>
                  );       

                  if (!this._isNew() && authToken.can_update_repairs) {
                    deleteButtonNode = (
                      <div className={s.deleteWrapper}>
                        <Button id={'rep_delete'} onClick={this._destroy} small dangerous>Delete repair</Button>
                      </div>
                    );
                  }           
                }

                return (
                  <div>
                    <ErrorList errors={this._errors()} />
                    {formNode}
                    {deleteButtonNode}
                  </div>
                );
              }}
            />
          </div>
          <div className={s.commentsSide}>
            {commentsNode}
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  openRepairs: () => dispatch(openRepairs()),
});

const mapStateToProps = state => ({
  repairsFilter: getRepairsFilter(state),
  authToken: getAuthToken(state),
});

Repair = compose(
  graphql(RepairQuery, {
    skip: ({ match }) => match.params.id === 'new',
    options: ({ match }) => ({
      variables: { id: match.params.id },
    }),
    name: 'repairData',
  }),
  graphql(UsersQuery, { name: 'usersData' }),
  graphql(CreateRepairQuery, { name: 'createRepair' }),
  graphql(UpdateRepairQuery, { name: 'updateRepair' }),
  graphql(RepairDeleteQuery, { name: 'deleteRepair' }),
  connect(mapStateToProps, mapDispatchToProps)
)(Repair);

export default Repair;