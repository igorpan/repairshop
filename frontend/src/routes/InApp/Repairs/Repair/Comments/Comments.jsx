import React, { Component } from 'react';
import { TextInput } from 'Form';
import { SpinnerOrRender } from 'Spinner';
import s from './Comments.scss';
import { graphql, compose } from 'react-apollo';
import {
  RepairCommentsQuery,
  CreateRepairCommentQuery,
} from 'util/queries';

class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newCommentText: '',
    };
  }

  _createComment = () => {
    const { newCommentText } = this.state;
    const { createRepairComment, repairId } = this.props;

    if (newCommentText.trim() !== '') {
      createRepairComment({
        variables: { text: newCommentText, repair_id: repairId },
        refetchQueries: [{ query: RepairCommentsQuery, variables: { repair_id: repairId } }],
      }).then(() => {
        this.setState({ newCommentText: '' });
      });
    }
  };

  render() {
    const { newCommentText } = this.state;
    const { commentsData } = this.props;

    if (commentsData.error) {
      return <div>Error fetching comments :-(</div>;
    }

    let commentsNode = (
      <SpinnerOrRender
        loading={commentsData.loading}
        render={() => {
          if (commentsData.repair.comments.length > 0) {
            return (
              <ul>
                {commentsData.loading ? [] : commentsData.repair.comments.map(comment => (
                  <li className={s.comment} key={comment.id}>
                    <span className={s.author}>{comment.author.name}:</span>
                    {comment.text}
                  </li>
                ))}
              </ul>
            );
          } else {
            return <div>No comments have been left on this repair.</div>;
          }
        }}
      />
    );

    return (
      <div>
        <div className={s.comments}>
          {commentsNode}    
        </div>
        <div>
          <TextInput 
            disabled={commentsData.loading}
            value={newCommentText} 
            onChange={v => this.setState({ newCommentText: v })}
            onKeyDown={e => {
              if (e.which === 13) {
                this._createComment();
              }
            }}
            placeholder={'Comment on this repair...'}
          />
        </div>
      </div>
    );
  }
} 

Comments = compose(
  graphql(RepairCommentsQuery, {
    options: ({ repairId }) => ({
      variables: { repair_id: repairId }
    }),
    name: 'commentsData',
  }),
  graphql(CreateRepairCommentQuery, {
    name: 'createRepairComment',
  })
)(Comments);

export default Comments;