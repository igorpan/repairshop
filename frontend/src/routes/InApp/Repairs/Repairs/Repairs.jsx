import React, { Component } from 'react';
import PageHeader from 'PageHeader';
import Table from 'Table';
import Toolbar, { ToolbarInputCell } from 'Toolbar';
import { Form, FormControl, Select, DateTimePicker } from 'Form';
import Button from 'Button';
import { SpinnerOrRender } from 'Spinner';
import s from './Repairs.scss';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import moment from 'moment';
import { getRepairsFilter, getAuthToken } from 'util/redux/selectors';
import { 
  changeRepairsFilter, 
  openRepairEditor,
} from 'util/redux/actions';
import { RepairsQuery } from 'util/queries';


class Repairs extends Component {
  _changeFilter = newFilter => {
    const { changeFilter } = this.props;
    changeFilter(newFilter);
  };

  render() {
    const { data, openRepairEditor, authToken } = this.props;

    const filter = data.variables;

    let assigneeFilterNode;
    if (authToken.can_see_users) {
      assigneeFilterNode = (
        <ToolbarInputCell
          label={'Assignee'}
          inputNode={
            <FormControl
              control={Select}
              path={'assignee_id'}
              controlProps={{
                options: data.loading ? [{ value: null, label: 'Anyone' }] : [{ value: null, label: 'Anyone' }, ...data.users.map(user => ({ value: user.id, label: user.name }))]
              }}
            />
          }
        />
      );
    }

    let newRepairButtonNode;
    if (authToken.can_update_repairs) {
      newRepairButtonNode = (
        <Button id={'new_repair'} onClick={() => openRepairEditor()}>New repair</Button>
      );
    }

    return (
      <div>
        <PageHeader 
          title={'Repairs'}
          actionContent={
            <div>
              {newRepairButtonNode}
            </div>
          }
        />
        <Form value={filter} onChange={this._changeFilter}>
          <Toolbar>
            <ToolbarInputCell 
              label={'From'}
              inputNode={
                <FormControl
                  control={DateTimePicker}
                  path={'from_time'}
                  controlProps={{
                    selectsStart: true,
                    startDate: filter.from_time,
                    endDate: filter.to_time,
                  }}
                />
              }
            />
            <ToolbarInputCell 
              label={'To'}
              inputNode={
                <FormControl
                  control={DateTimePicker}
                  path={'to_time'}
                  controlProps={{
                    selectsEnd: true,
                    startDate: filter.from_time,
                    endDate: filter.to_time,
                  }}
                />
              }
            />
            <ToolbarInputCell
              label={'Status'}
              inputNode={
                <FormControl
                  control={Select}
                  path={'status'}
                  controlProps={{
                    options: [
                      { value: null, label: 'Any' },
                      { value: 'incomplete', label: 'Incomplete' }, 
                      { value: 'complete', label: 'Complete' },
                      { value: 'approved', label: 'Complete&Approved' }
                    ]
                  }}
                />
              }
            />
            {assigneeFilterNode}
          </Toolbar>
        </Form>
        <SpinnerOrRender 
          loading={data.loading}
          render={() => (
            <Table 
              columns={[
                {
                  label: 'Description',
                  width: '40%'
                },
                {
                  label: 'Time',
                  width: '20%'
                },
                {
                  label: 'Status',
                  width: '15%'
                },
                {
                  label: 'Assignee',
                  width: '15%'
                },
                {
                  label: '',
                  width: '10%',
                }
              ]}
              rows={data.repairs.map(repair => [
                repair.description, 
                moment(repair.time).format('MMM D, YY hh:mmA'), 
                repair.status, 
                repair.assignee ? data.users.find(u => u.id === repair.assignee.id).name : 'Unassigned',
                () => (
                  <div className={s.actions}>
                    <Button small onClick={() => openRepairEditor(repair.id)}>View</Button>
                  </div>
                )
              ])}
            />
          )}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filter: getRepairsFilter(state),
  authToken: getAuthToken(state),
});

const mapDispatchToProps = dispatch => ({
  changeFilter: (newFilter) => dispatch(changeRepairsFilter(newFilter)),
  openRepairEditor: (id) => dispatch(openRepairEditor(id)),
});

Repairs = compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(RepairsQuery, {
    options: ({ filter }) => ({
      variables: filter,
    })
  }),
)(Repairs);

export default Repairs;