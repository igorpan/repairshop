import React from 'react';
import Menu from './Menu';
import s from './Layout.scss';

const Layout = ({ children }) => (
  <div className={s.wrapper}>
    <div className={s.menu}>
      <Menu />
    </div>
    <div className={s.content}>
      {children}
    </div>
  </div>
);

export default Layout;