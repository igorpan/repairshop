import React from 'react';
import s from './Menu.scss';
import logo from './logo.png';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { logout } from 'util/redux/actions';
import { getAuthToken } from 'util/redux/selectors';

const MenuLinkItem = ({ children, to }) => (
  <li className={s.linkItem}>
    <NavLink to={to} activeClassName={s.active}>{children}</NavLink>
  </li>
);

let Menu = ({ logout, authToken }) => {
  let userManagementNode;
  if (authToken.can_see_users) {
    userManagementNode = (
      <MenuLinkItem to={'/users'}>
        User management
      </MenuLinkItem>
    );
  }

  return (
    <div className={s.wrapper}>
      <div className={s.header}>
        <img className={s.logo} src={logo} alt={'logo'} />
        Repair shop
      </div>
      <div className={s.linksContainer}>
        <ul>
          <MenuLinkItem to={'/repairs'}>
            Repairs
          </MenuLinkItem>
          {userManagementNode}
        </ul>
        <ul>
          <MenuLinkItem to={'/logout'}>
            Logout
          </MenuLinkItem>
        </ul>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  authToken: getAuthToken(state),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

Menu = withRouter(connect(mapStateToProps, mapDispatchToProps)(Menu));

export default Menu;