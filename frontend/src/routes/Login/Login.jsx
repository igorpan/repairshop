import React, { Component }  from 'react';
import {
  Form,
  FormControl,
  TextInput,
} from 'Form';
import Button from 'Button';
import LoginLogo from './LoginLogo';
import s from './Login.scss';
import classNames from 'classnames';
import { gql, withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import { login, openApp } from 'util/redux/actions';

const AuthenticateQuery = gql`query auth_token($email: String!, $password: String!){ 
  auth_token(email: $email, password: $password){ 
    jwt
    can_see_users
    can_update_repairs
  }
}`;

const STATE_IDLE = 'IDLE';
const STATE_LOGGING_IN = 'LOGGING_IN';
const STATE_LOGIN_ERROR = 'LOGIN_ERROR';
const STATE_SYSTEM_ERROR = 'SYSTEM_ERROR';

const errorStrings = {
  [STATE_LOGIN_ERROR]: 'Oops, that doesn\'t look right.',
  [STATE_SYSTEM_ERROR]: 'Hmm...system error. Please try again later.',
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        email: '',
        password: '',
      },
      logoTicks: 0,
      state: STATE_IDLE,
    };
  }

  _changeFormData = newData => {
    const { logoTicks } = this.state;

    this.setState({ formData: newData, logoTicks: logoTicks + 1 });
  };

  _login = () => {
    const { client, login } = this.props;
    const { formData } = this.state;
    this.setState({ state: STATE_LOGGING_IN });
    client.query({
      query: AuthenticateQuery,
      variables: { email: formData.email, password: formData.password },
    })
      .then(result => {
        const { auth_token } = result.data;
        if (auth_token) {
          this.setState({ state: STATE_IDLE });
          login(auth_token);
        } else {
          this.setState({ state: STATE_LOGIN_ERROR });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ state: STATE_SYSTEM_ERROR });
      });
  };

  render() {
    const { formData, logoTicks, state } = this.state;

    return (
      <div className={s.wrapper}>
        <div className={s.panel}>
          <div className={s.logo}>
            <LoginLogo
              angle={logoTicks * 30}
              spin={state  === STATE_LOGGING_IN}
            />
          </div>

          <Form
            value={formData}
            onChange={this._changeFormData}
            onSubmit={this._login}
          >
            <div className={s.row}>
              <FormControl
                control={TextInput}
                controlProps={{ type: 'email', placeholder: 'Email', required: true }}
                path={'email'}
              />
            </div>
            <div className={s.row}>
              <FormControl
                control={TextInput}
                controlProps={{ type: 'password', placeholder: 'Password', required: true }}
                path={'password'}
              />
            </div>
            <div className={classNames(s.row, s.errorBox)}>
              {errorStrings[state] || ''}
            </div>
            <div className={s.row}>
              <Button type={'submit'}>Login</Button>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  login: (token) => dispatch(login(token)),
  openApp: () => dispatch(openApp()),
});

Login = connect(null, mapDispatchToProps)(withApollo(Login));

export default Login;
