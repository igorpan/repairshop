import React from 'react';
import logoImg from './login_logo.png';
import s from './LoginLogo.scss';

const LoginLogo = ({ angle, spin }) => (
  <img
    src={logoImg}
    alt={'Repair shop logo'}
    className={spin ? s.spin : ''}
    style={{
      transform: `rotate(${angle || 0}deg)`,
      transitionDuration: '0.3s',
    }}
  />
);

export default LoginLogo;