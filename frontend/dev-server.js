var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');
var path = require('path');

new WebpackDevServer(webpack(config), {
  contentBase: path.join(__dirname, 'public'),
  publicPath: '/',
  port: 3001,
  historyApiFallback: true,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
    "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
  },
  disableHostCheck: true
}).listen(3001, '0.0.0.0', function (err, result) {
  if (err) {
    return console.log(err);
  }
});